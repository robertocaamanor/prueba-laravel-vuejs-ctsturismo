<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleDescsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_descs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('crashRating');
            $table->string('description');
            $table->bigInteger('vehicleId')->unsigned();
            $table->timestamps();

            $table->foreign('vehicleId')->references('id')->on('vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_descs');
    }
}
