<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleDesc extends Model
{
    protected $id;

    protected $fillable = [
        'description', 'crashRating', 'vehicleId'
    ];
}
