<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
    protected $id;

    protected $fillable = [
        'modelYear', 'manufacturer', 'model'
    ];
}
