<?php

namespace App\Http\Controllers;

use App\Vehicles;
use Illuminate\Http\Request;

class VehiclesController extends Controller
{
    public function displayVehicles(){
        $vehicles = Vehicles::all();

        $veh = $vehicles->map(function ($ve) {
            return $ve->only(['modelYear', 'manufacturer', 'model']);
        });

        return $veh;
    }
}
