<?php

namespace App\Http\Controllers;

use App\VehicleDesc;
use Illuminate\Http\Request;

class VehicleDescController extends Controller
{
    public function byModelManMod($modelYear, $manufacturer, $model, Request $request){
        $withRating = $request->withRating;


        $vehicleDesc = VehicleDesc::leftJoin('vehicles', 'vehicles.id', '=', 'vehicle_descs.vehicleId')
        ->where('vehicles.modelYear', $modelYear)
        ->where('vehicles.manufacturer', $manufacturer)
        ->where('vehicles.model', $model)->get();

        if($withRating == 'true'){
            $vd = $vehicleDesc->map(function ($ve_de) {
                return $ve_de->only(['crashRating','description', 'vehicleId']);
            });
        }else{
            $vd = $vehicleDesc->map(function ($ve_de) {
                return $ve_de->only(['description', 'vehicleId']);
            });
        }

        

        return [
            'Count' => $vehicleDesc->count(),
            'Results' => $vd
        ];
    }
}
