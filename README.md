# Instrucciones

1) Clona este repositorio
2) Importa el archivo "ctsturismo.sql" en tu servidor XAMPP
3) Ejecuta "npm install" para instalar las dependencias
4) Ejecuta "npm run dev" para ejecutar el mezclador Webpack
5) Ejecuta "composer install" par ejecutar el instalador de Vendor
6) Ejecuta "php artisan serve" para inicializar el servidor local y ejecutar las pruebas

